# Como trabajar con formulario es react - II:

Seguimos trabajando con el formulario de inicio de sesión, tenemos los inputs que estan relacionados con un estado.

Hemos visto como poner los atributos value={} y onChange={}. Pero hay mas cosas que podemos hacer con un formulario.

Vamos a ver como hacer validaciones de los datos que vienen del formulario, tambien tenemos que ver como evitar que se envien los datos al hacer click al boton de enviar, para poder tener control de los datos de los input y de los datos para nosotros decidir que hacer con ellos.

Vamos a trabajar en el archivo FormularioSesion.jsx.

Vamos a ver como podemos hacer un refactoring de las funciones que obtienen los datos de los input, ahora solo tenemos 2, pero imagina que son 10, no podemos tener 10 funciones, no es viable. Haremos una optimizacion del código.

Lo que haremos es crear una sola funcion onChange, que internamente podemos comprobar el tipo de input con e.target.name.

## FormularioSesion.jsx:

```javascript
// Creamos la funcion global para todos los input:
// Con e.targe.name accedemos al input:
const onChange = (e) => {
  if (e.target.name === 'user') {
    changeUser(e.target.value);
  } else if (e.target.name === 'password') {
    changePassword(e.target.value);
  }
};
```

Ahora vamos a ver la validaciones, cuando hagamos click en el boton de enviar podemos colocar el atributo onChange={} cuando se envie el formulario. Osea cuando hagamos un evento de tipo onSubmit={} queremos hacer las validaciones. A nuestro formulario colocamos ese evento.

<font style="color: red; font-size:18px">
NOTA:
</font>
Estas validaciones es solo para probar como funciona, pero asi no se hacen las validaciones ya que este código esta en el frontend y cualquiera podria verlo, lo que hacemos es comprobar como funciona la lógica del código.

Al formulario debemos colocarle un evento, el evento onSubmit={}, recibe una función, esta funcion normalmente la llamamos igual, onSubmit, ahi podemos prevenir el envio por defecto del formulario con el método e.preventDefault(), luego ya con un simple if podemos saber si los valores que recibe el input son iguales a los valores que nosotros tenemos llamamos la funcion que viene por props uqe cambia el estado.

## Formuario y evento onSubmit={}:

```javascript
<form id="form" onSubmit={onSubmitForm}>
  <div>
    <label htmlFor="user">Usuario</label>
    <input type="text" id="user" name="user" value={user} onChange={onChange} />
  </div>
  <div>
    <label htmlFor="password">Contraseña</label>
    <input
      type="password"
      id="password"
      name="password"
      value={password}
      onChange={onChange}
    />
  </div>
  <button type="submit">Iniciar sesión</button>
</form>
```

## Función onSubmitForm():

```javascript
// Funcion que se ejcuta dar click al boton de envia el formulario:
const onSubmitForm = (e) => {
  // Prevenimos el comportamiento por defecto del formulario:
  e.preventDefault();

  // Hacemos las validaciones:
  if (user === 'leonardo' && password === '123') {
    // Lanzamos la funcion que cambia el estado a true que viene por props.
    iniciarSesion();
  } else {
    alert('Datos incorrectos.');
    // Datos incorrectos limpiamos el formaulario:
    changeUser('');
    changePassword('');
  }
};
```

La funcion iniciarSesion() viene por props del formulario:

```javascript
const FormularioSesion = ({ iniciarSesion }) => {};
```

Vemos como pasamos por props al llamar el componente en app.jsx:

## App.jsx

```javascript
import { useState } from 'react';
import Usuario from './components/Usuario';
import FormularioSesion from './components/FormularioSesion';

const App = () => {
  /*
  Creamos el estado inicial de nuestro componente
  y por medio de changeSesion() lo cambiamos:
  */
  const [sesion, changeSesion] = useState(false);
  const cerrarSesion = () => {
    changeSesion(false);
  };
  const iniciarSesion = () => {
    changeSesion(true);
  };

  return (
    <>
      {sesion ? (
        <>
          <Usuario />
          <button onClick={cerrarSesion}>Cerrar Sesión</button>
        </>
      ) : (
        <>
          <p>No has iniciado sesion.</p>
          <FormularioSesion iniciarSesion={iniciarSesion} />
        </>
      )}
    </>
  );
};

export default App;
```
