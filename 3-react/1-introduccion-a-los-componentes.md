# Introducción a los componentes:

Vamos a ver como organizar en componentes nuestra aplicacion, los componentes nos va a permitir organizar nuestro código en pequeños fragmentos de código reutilizable.

Un componente es una funcion que retorna codigo jsx. Lo podemos ver en nuestro componente llamado Usuario.jsx dentro de App.jsx.

Los componentes deben seguir unas reglas:

- El nombre del componente debe empezar en mayuscula,
- Si nuestro componente es de mas de una palabra usaremos PascalCase. UsuarioTestOne,

Vamos a ver como podemos usar el componente varias veces solo debemos llamarlo en App.jsx las veces que queramos.

El componente lo podemos llamar como una etiqueta HTML. Vemos como podemos duplicar el componente y solo con cambiar en un solo sitio todos los componentes se actualizan.

Y nuestro componente App.jsx vemos como llamamos las veces que queramos el componente Usuario:

```javascript
const App = () => {
  const sesion = true;

  return (
    <>
      {sesion ? (
        <>
          <Usuario />
          <Usuario />
          <Usuario />
          <Usuario />
        </>
      ) : (
        <p>No has iniciado sesion.</p>
      )}
    </>
  );
};

// Componente Usuario:
const Usuario = () => {
  const nombre = 'Leonardo';
  const color = 'crimson';
  const pais = 'Polonia';
  const amigos = ['Alejandro', 'Manuel', 'Camila'];
  return (
    <div>
      <h1 style={{ color: color }} className="title">
        Hola {nombre}
      </h1>
      <p>Que tengas un buen dia.</p>
      {pais && <p>Tu eres de {pais}.</p>}
      <p>Mis amigos son:</p>
      <ul>
        {amigos.map((amigo, index) => {
          return <li key={index}>{amigo}</li>;
        })}
      </ul>
      <p>Que tengas un buen dia</p>
    </div>
  );
};
```

De momento lo tenemos todo en solo archivo, lo que veremos a continuacion como organizar mejor en carpetas los componentes.
