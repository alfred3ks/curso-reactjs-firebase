# Componentes en múltiples archivos:

Vamos a ver como organizar los componentes en múltiples archivos. Separar los componentes en archivos independientes.

Para esto creamos una carpeta llamada components, la pondremos dentro de la carpets src.

Dentro de la carpeta components ya podemos crear nuestros componentes.

Creamos el componente Usuario.jsx:

## Usuario.jsx:

```javascript
// Componente Usuario:
const Usuario = () => {
  const nombre = 'Leonardo';
  const color = 'crimson';
  const pais = 'Polonia';
  const amigos = ['Alejandro', 'Manuel', 'Camila'];
  return (
    <div>
      <h1 style={{ color: color }} className="title">
        Hola {nombre}
      </h1>
      <p>Que tengas un buen dia.</p>
      {pais && <p>Tu eres de {pais}.</p>}
      <p>Mis amigos son:</p>
      <ul>
        {amigos.map((amigo, index) => {
          return <li key={index}>{amigo}</li>;
        })}
      </ul>
      <p>Que tengas un buen dia</p>
    </div>
  );
};

export default Usuario;
```

Ahora importamos ese componente en App.jsx:

## App.jsx:

```javascript
import Usuario from './components/Usuario';

const App = () => {
  const sesion = true;
  return (
    <>
      {sesion ? (
        <>
          <Usuario />
        </>
      ) : (
        <p>No has iniciado sesion.</p>
      )}
    </>
  );
};

export default App;
```

Ahora veremos como el titulo pasarlo a ser un componente. Lo llamaremos Titulo.jsx:

## Titulo.jsx:

```javascript
const Titulo = () => {
  const nombre = 'Leonardo';
  const color = 'crimson';
  return (
    <h1 style={{ color: color }} className="title">
      Hola {nombre}
    </h1>
  );
};

export default Titulo;
```

Ahora ese componente lo podemos usar las veces que queramos:

## Usuario.jsx:

```javascript
import Titulo from './Titulo';

// Componente Usuario:
const Usuario = () => {
  const pais = 'Polonia';
  const amigos = ['Alejandro', 'Manuel', 'Camila'];
  return (
    <div>
      <Titulo />
      <Titulo />
      <Titulo />
      <p>Que tengas un buen dia.</p>
      {pais && <p>Tu eres de {pais}.</p>}
      <p>Mis amigos son:</p>
      <ul>
        {amigos.map((amigo, index) => {
          return <li key={index}>{amigo}</li>;
        })}
      </ul>
      <p>Que tengas un buen dia</p>
    </div>
  );
};

export default Usuario;
```

Luego veremos las props y podemos ver como hacer que los componente que aunque lo usamos en diferentes partes este sea dinamico, en funcion de unos parametros que le pasemos este se mostrara de diferente manera.
