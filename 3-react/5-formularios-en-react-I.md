# Como trabajar con formulario es react - I:

Vamos a ver como trabajar con formularios dentro de React.

Crearemos un nuevo componente que sera de nuestro formulario. Lo crearemos dentro de la carpeta components.

Lo llamaremos FormularioSesion.js

<font style="color: red; font-size:18px">
NOTA:
</font>
En react el elemento label no usa el atributo for, usa htmlFor=" ", eso porque la palabra for es una palabra reservada del lenguaje.

Aqui vamos a trabajar con los eventos de formularios. En nuestro elemento input debemos colocar unos atributos importantes. El atributo value={}, que es el valor que recibira actual el imput y el atributo onChance={} el cual va a recibir una funcion que normalmente se le suele llamar handle, para nuestro caso la llamaremos onChangeUser para el usuario y onChagePassword para el password, estas funciones las creamos fuera y reciben por parametro el evento, y por medio de este podemos acceder al e.target.value. Con esto cuando metamos valores en el input iremos capturando ese contenido.

Al ejecutarse esas funciones se ejecutara dentro que le pasamos la funcion que cambia el estado.

<font style="color: red; font-size:18px">
NOTA:
</font>
Aqui lo importante que hemos visto hoy soy los dos atributos que puede recibir el inpunt el value={} y el onChange={}. Tambien que por medio del evento podemos obtener el valor del inpur, e.target.value.

Asi queda nuestro componente FormularioSesion.jsx:

## FormularioSesion.jsx:

```javascript
import { useState } from 'react';

// eslint-disable-next-line react/prop-types
const FormularioSesion = ({ iniciarSesion }) => {
  // Definimos los estados inicial de los imput:
  const [user, changeUser] = useState('');
  const [password, changePassword] = useState('');

  const onChangeUser = (e) => {
    // Actualizamos el estado del input al meter datos:
    // Con e.target.value obtenemos el valor que tiene el input
    changeUser(e.target.value);
  };
  const onChangePassword = (e) => {
    changePassword(e.target.value);
  };

  return (
    <form id="form">
      <p>Usuario:{user}</p>
      <p>Contraseña:{password}</p>
      <div>
        <label htmlFor="user">Usuario</label>
        <input
          type="text"
          id="user"
          name="user"
          value={user}
          onChange={onChangeUser}
        />
      </div>
      <div>
        <label htmlFor="password">Contraseña</label>
        <input
          type="password"
          id="password"
          name="password"
          value={password}
          onChange={onChangePassword}
        />
      </div>
      <button onClick={iniciarSesion} type="submit">
        Iniciar sesión
      </button>
    </form>
  );
};

export default FormularioSesion;
```

Vemos como lo que deciamos el input, del user:

```javascript
<input type="text" id="user" name="user" value={user} onChange={onChangeUser} />
```

Luego tenemos la funcion onChanceUser que es la que recibe el evento y donde ejecutamos el cambio del estado del input cada vez que metemos un dato en el.

```javascript
const onChangeUser = (e) => {
  // Actualizamos el estado del input al meter datos:
  // Con e.target.value obtenemos el valor que tiene el input
  changeUser(e.target.value);
};
```

Muy muy interesante...
