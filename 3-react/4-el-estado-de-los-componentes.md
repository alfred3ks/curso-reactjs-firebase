# El estado de los componentes:

El estado es muy importante para los componentes y en general para todo React y las aplicaciones porque nos permite mantener que es lo que queremos mostrar en pantalla al usuario.

Vamos a ver un ejemplo práctico.

En este ejemplo veremos por medio de un boton que podemos cambiar el estado de nuestro componente.

Pondremos un boton de iniciar sesion y otro de cerrar la sesion.

Por medio de esos botones es que vamos a cambiar el estado del componente.

A los botones vamos a poner un evento, el evento OnClick. Para trabajar con estado en react debemos usar un hook, useState().

Siempre, pero siempre que el estado cambie se va a recargar el componente. Vemos como en el componente App.jsx implementado el estado.

Tenemos que importarlo de react en la cabecera:

```javascript
import { useState } from 'react';
```

Luego para poder usar estado lo tenemos que hacer dentro del componente, sino no va a funcionar.

La estructura de usaState es la siguiente:

```javascript
const [sesion, changeSesion] = useState(true);
```

Hacemos destructuring, la primera variable, sesion toma el valor que le pasemos a useState(true), y luego cuando queramos cambiar la variable sesion a false, simlemente llamamos changeSesion(false) y le pasamos el valor que deseamos que es false.

Como nuestro caso para cambiar el estado lo hemos realizado con un boton, a ese boton le colocamos una funcion que disparara una funcion. Vemos como nos queda nuestro componente App.jsx implementado el estado.

## App.jsx:

```javascript
import { useState } from 'react';
import Usuario from './components/Usuario';

const App = () => {
  /*
  Creamos el estado inicial de nuestro componente
  y por medio de changeSesion() lo cambiamos:
  */
  const [sesion, changeSesion] = useState(true);

  const cerrarSesion = () => {
    changeSesion(false);
  };

  const iniciarSesion = () => {
    changeSesion(true);
  };

  return (
    <>
      {sesion ? (
        <>
          <Usuario />
          <button onClick={cerrarSesion}>Cerrar Sesión</button>
        </>
      ) : (
        <>
          <p>No has iniciado sesion.</p>
          <button onClick={iniciarSesion}>Iniciar Sesión</button>
        </>
      )}
    </>
  );
};

export default App;
```

Ahora cuando probamos los botones vemos como cambia la aplicación de un estado de true a false, con solo dar click a los botones.
