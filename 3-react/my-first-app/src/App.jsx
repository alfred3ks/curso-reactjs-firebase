import { useState } from 'react';
import Usuario from './components/Usuario';
import FormularioSesion from './components/FormularioSesion';
import Counter from './components/Counter';
import CounterClass from './components/CounterClass';

const App = () => {
  /*
  Creamos el estado inicial de nuestro componente
  y por medio de changeSesion() lo cambiamos:
  */
  const [sesion, changeSesion] = useState(false);
  const cerrarSesion = () => {
    changeSesion(false);
  };
  const iniciarSesion = () => {
    changeSesion(true);
  };

  return (
    <>
      {sesion ? (
        <>
          <Usuario />
          <Counter incremento={100} decremento={30} />
          <CounterClass incremento={10} decremento={3} />
          <button onClick={cerrarSesion}>Cerrar Sesión</button>
        </>
      ) : (
        <>
          <p>No has iniciado sesion.</p>
          <FormularioSesion iniciarSesion={iniciarSesion} />
        </>
      )}
    </>
  );
};

export default App;
