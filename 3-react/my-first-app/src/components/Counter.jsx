import { useState } from 'react';

const Counter = (props) => {
  const [counter, changeCounter] = useState(0);

  // eslint-disable-next-line react/prop-types
  const { incremento, decremento } = props;

  const incrementar = (incremento) => {
    changeCounter(counter + incremento);
  };

  const disminuir = (decremento) => {
    changeCounter(counter - decremento);
  };

  return (
    <div>
      <h1>Counter: {counter}</h1>
      <button
        onClick={() => {
          incrementar(incremento);
        }}
      >
        Incrementar
      </button>
      <button
        onClick={() => {
          disminuir(decremento);
        }}
      >
        Disminuir
      </button>
    </div>
  );
};

export default Counter;
