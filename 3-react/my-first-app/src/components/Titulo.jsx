// Destructuracion del objeto props:
const Titulo = (props) => {
  // eslint-disable-next-line react/prop-types
  const { usuario = 'Usuario', color } = props;
  return (
    <>
      <h1 style={{ color: color }} className="title">
        Hello {usuario}!!!
      </h1>
    </>
  );
};

export default Titulo;
