import Titulo from './Titulo';

// Componente Usuario:
const Usuario = () => {
  const pais = 'Suiza';
  const amigos = ['Alejandro', 'Manuel', 'Camila'];
  return (
    <div>
      <Titulo usuario="Leonardo" color={'crimson'} edad={34} />
      <p>Mis amigos son:</p>
      <ul>
        {amigos.map((amigo, index) => {
          return <li key={index}>{amigo}</li>;
        })}
      </ul>
      {pais && <p>Soy de {pais}.</p>}
      <p>Que tengas un buen dia</p>
    </div>
  );
};

export default Usuario;
