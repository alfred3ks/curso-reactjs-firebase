# Componentes basados en clases:

Hasta ahora hemos visto los componentes funcionales, aunque son los componentes mas usado a dia de hoy, pero vamos a ver como son los componentes basados en clases. Sobre todo por código legacy.

Para eso vamos a crear un componente contador llamado Counter.jsx.

Para crear un componente basado en clases tenemos que usae la palabra class, nombre del componente y usar la palabra extends de Component. Debemos importar de React Component.

Ahora para poder mostrar JSX debemos usar el metodo render(){}, donde ya podemos retornar jsx.

```javascript
import { Component } from 'react';

// Creamos una clase:
class Counter extends Component {
  render() {
    return (
      <div>
        <h1>Counter</h1>
      </div>
    );
  }
}

export default Counter;
```

Lo importamos en App.jsx. Vemos que tenemos como establecemos las props y el estado con el método constructor. El estado lo estableemos con this.stte y le pasamos un objeto. Algo bastante mas complicado como a dia de hoy.

Asi quedaria nuestro contador de clase:

## CounterClass.jsx:

```javascript
import { Component } from 'react';

// Creamos una clase:
class CounterClass extends Component {
  // metodo para ver las props/estado:
  constructor(props) {
    super(props);

    // Asi establecemos el estado:
    this.state = { counter: 0 };
  }

  // Métodos del contador
  incrementar() {
    // Asi cambiamos el estado:
    this.setState((estadoAnterior) => {
      return {
        counter: estadoAnterior.counter + 1,
      };
    });
  }

  disminuir() {
    this.setState((estadoAnterior) => {
      return {
        counter: estadoAnterior.counter - 1,
      };
    });
  }

  // Usamos el metodo render() para retornar jsx
  render() {
    return (
      <div>
        <h1>Counter: {this.state.counter}</h1>
        <button
          onClick={() => {
            this.incrementar();
          }}
        >
          Incrementar
        </button>
        <button
          onClick={() => {
            this.disminuir();
          }}
        >
          Disminuir
        </button>
      </div>
    );
  }
}

export default CounterClass;
```

Tambien podemos ver que si queremos pasar por props las cantidades a incrementar o disminuir los podemos hacer:

```javascript
<CounterClass incremento={10} decremento={3} />
```

Las props las recibimos dentro del constuctor como hemos visto ya tenemos acceso a las props y ya podemos pasarlas a las funciones de incremento y decremento.

```javascript
constructor(props) {
    super(props);
  }
```

```javascript
<button
  onClick={() => {
    this.incrementar(this.props.incremento);
  }}
>
  Incrementar
</button>

  // Métodos del contador
  incrementar(incremento) {
    // Asi cambiamos el estado:
    this.setState((estadoAnterior) => {
      return {
        counter: estadoAnterior.counter + incremento,
      };
    });
  }
```

Igual hacemos para el decremento.
