# Las props de los componentes:

A los componentes podemos hacerlos reutilizables y hacerlos mas dinámicos, para eso tenemos las props.

Las props es un objeto {}, que se lo pasamos al componente. Vamos a ver como pasar props a nuestro componente Titulo.jsx

Desde nuestro componente podemos enviar propiedades para que sena renderizadas de manera dinámica, podemos ver que hemos mostrado varias veces el componente titulo pero con diferentes props.

A traves de las propiedades(props) podemos enviar string, number, arreglos [], o objetos {}. Para nuestro ejemplo hemos enviado string.

Dependera la forma como enviemos lo props la forma como podemos mostrar esas propiedades en el componente.

al recibir las propiedades en el componente podemos hacer destructuring del objeto props y obtener las propiedades.

Asi nos quedaria nuestro componente Usuario.jsx con diferentes titulos:

```javascript
import Titulo from './Titulo';

// Componente Usuario:
const Usuario = () => {
  const pais = 'Suiza';
  const amigos = ['Alejandro', 'Manuel', 'Camila'];
  const dataUser = { name: 'Maykel', color: 'red', age: 25 };

  return (
    <div>
      <Titulo usuario="Leonardo" color={'crimson'} edad={34} />
      <Titulo usuario={'Layka'} color={'blue'} />
      <Titulo usuario={'Luis'} />
      <Titulo usuario={usuario.nombre} edad={usuario.edad} />
      <p>Que tengas un buen dia.</p>
      {pais && <p>Soy de {pais}.</p>}
      <p>Mis amigos son:</p>
      <ul>
        {amigos.map((amigo, index) => {
          return <li key={index}>{amigo}</li>;
        })}
      </ul>
      <p>Que tengas un buen dia</p>
    </div>
  );
};

export default Usuario;
```

Y asi queda nuestro componente Titulo.jsx:

```javascript
// Destructuracion del objeto props:
const Titulo = (props) => {
  // eslint-disable-next-line react/prop-types
  const { usuario = 'Usuario', color, edad } = props;
  return (
    <>
      <h1 style={{ color: color }} className="title">
        Hello soy {usuario}
      </h1>
      <p>{edad && <>Tengo {edad} años.</>}</p>
    </>
  );
};

export default Titulo;
```

Voy a crear un componente llamado MiComponente.jsx y le voy a pasar un objeto para ver como desestructurar el objeto y mostrar la informacion que viene por props del objeto.

## Usuario.jsx:

```javascript
import Titulo from './Titulo';
import GetUserObj from './GetUserObj';

// Componente Usuario:
const Usuario = () => {
  const pais = 'Suiza';
  const amigos = ['Alejandro', 'Manuel', 'Camila'];
  const usuario = { nombre: 'Maykel', edad: 25 };
  const dataUser = { name: 'Maira', color: 'red', age: 45 };
  return (
    <div>
      <Titulo usuario="Leonardo" color={'crimson'} edad={34} />
      <Titulo usuario={'Layka'} color={'blue'} />
      <Titulo usuario={'Luis'} />
      <Titulo usuario={usuario.nombre} edad={usuario.edad} />
      <p>Que tengas un buen dia.</p>
      {pais && <p>Soy de {pais}.</p>}
      <p>Mis amigos son:</p>
      <ul>
        {amigos.map((amigo, index) => {
          return <li key={index}>{amigo}</li>;
        })}
      </ul>
      <p>Que tengas un buen dia</p>
      <GetUserObj usuario={dataUser} />
    </div>
  );
};

export default Usuario;
```

## GetUsuarioObj.jsx:

```javascript
// eslint-disable-next-line react/prop-types
const GetUserObj = ({ usuario }) => {
  // eslint-disable-next-line react/prop-types
  const { name, color, age } = usuario;
  return (
    <h2 style={{ color: color }} className="title">
      Hello soy {name} y tengo {age} años.
    </h2>
  );
};

export default GetUserObj;
```

Aqui lo que pasamos es:

```javascript
const dataUser = { name: 'Maira', color: 'red', age: 45 };
<GetUserObj usuario={dataUser} />

Esto seria lo mismo si hacemos esto:

<GetUserObj usuario={{ name: 'Maira', color: 'red', age: 45 }} />
```

Esto seria lo mismo si hacemos esto:

```javascript
const dataUser = { name: 'Maira', color: 'red', age: 45 };
<GetUserObj usuario={{ name: 'Maira', color: 'red', age: 45 }} />;
```

Y cuando hacemos destructuring lo que recibimos es el objeto props.usuario y hacemos destructuring {usuario} luego extraemos las propiedades por destructurin tambien.

```javascript
const GetUserObj = ({ usuario }) => {
  // eslint-disable-next-line react/prop-types
  const { name, color, age } = usuario;
```
