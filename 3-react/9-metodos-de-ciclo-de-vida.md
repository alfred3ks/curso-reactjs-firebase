# Métodos de ciclo de vida, Life Cycle Methods:

Vamos a ver los diferentes métodos de ciclo de vida de nuestros componentes.
Este tema es muy importante en React. sobre todo en los componentes basados en clases, en los componentes funcionales tambien es importante pero se trabaja de manera distinta. Se trabaja con hooks.

Lo de hoy lo veremos en el componente de CounterClass.jsx.

Cada vez que cambiamos el estado de un componente este se vuelve a renderizar.

Con los metodos de ciclo de vida podemos es ejecutar código en diferentes partes de nuestra aplicacion.

Cuando un componente desaparece del DOM, como nuestro caso que pulsamos cerrar sesion estamos quitando el componente CounterClass.jsx lo estamos quitando, esto se denomina desmontar el componente. Nosotros podemos ejecutar una funcion cuando este componente es desmontado, o por ejemplo cuando iniciamos sesion igual ejecutar una funcion cuando montamos de nuesvo el componente CounterClass.jsx, osea lo esta poniendo de nuevo en el DOM.

Vamos a ver como implementar esas funciones en funcion de lo que queramos.

Vamos a ver que cuando el contador se monte en pantalla, ejecutaremos un código.

- componentDidMount():

Para cuando se monta el contador ejecutamos este método.
Este método es muy cuando cuando por ejemplo queremos hacer una llamada a un servicio, a una API.

- componentDidUpdate(propsAnterior, stateAnterior):

Muy usado cuando el componente se vuelve a renderizar porque su estado cambio. Por ejemplo cuando pulsamos los botones de incremento y decremento.
Podriamos saber las propiedades anteriores del componente antes de actualizarlo y su estado, pasamos por parametros esos valores.

- componentWillUnmount() :

Este método nos permite ejecutar código antes de quitar el componente del DOM.
Lo probamos por ejemplo cerrando la sesion.
Tambien muy usado para cerrar conexion a la API.

## CounterClass.jsx:

```javascript
import { Component } from 'react';

// Creamos una clase:
class CounterClass extends Component {
  // metodo para ver las props/estado:
  constructor(props) {
    super(props);

    // Asi establecemos el estado:
    this.state = { counter: 0 };
  }

  // 📌 Metodos de ciclo de vida:
  componentDidMount() {
    /*
    Para cuando se monta el contador ejecutamos este método.
    Este método es muy cuando cuando por ejemplo queremos hacer una llamada a un servicio, a una API.
    */
    console.log('El contador se monto en el DOM.');
    // Llamamos a un API...
  }

  componentDidUpdate(propsAnterior, stateAnterior) {
    /*
    Muy usado cuando el componente se vuelve a renderizar porque su estado cambio. Por ejemplo cuando pulsamos los botones de incremento y decremento.
    Podriamos saber las propiedades anteriores del componente antes de actualizarlo y su estado, pasamos por parametros esos valores.
    */
    console.log('El contador se actualizo.');
    console.log(propsAnterior);
    console.log(stateAnterior);
  }

  componentWillUnmount() {
    /*
    Este método nos permite ejecutar código antes de quitar el componente del DOM.
    Lo probamos por ejemplo cerrando la sesion.
    Tambien muy usado para cerrar conexion a la API.
    */
    console.log('Adios componente!!!');
  }

  // Métodos del contador
  incrementar(incremento) {
    // Asi cambiamos el estado:
    this.setState((estadoAnterior) => {
      return {
        counter: estadoAnterior.counter + incremento,
      };
    });
  }

  disminuir(decremento) {
    this.setState((estadoAnterior) => {
      return {
        counter: estadoAnterior.counter - decremento,
      };
    });
  }

  // Usamos el metodo render() para retornar jsx
  render() {
    return (
      <div>
        <h1>Counter: {this.state.counter}</h1>
        <button
          onClick={() => {
            // eslint-disable-next-line react/prop-types
            this.incrementar(this.props.incremento);
          }}
        >
          Incrementar
        </button>
        <button
          onClick={() => {
            // eslint-disable-next-line react/prop-types
            this.disminuir(this.props.decremento);
          }}
        >
          Disminuir
        </button>
      </div>
    );
  }
}

export default CounterClass;
```
