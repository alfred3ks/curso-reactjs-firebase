# Como trabajar con estilos en React.js.

Vamos a ver como dar estilos css a los componentes y a toda la aplicación.

Veremos tres formas diferentes de trabajar con css en React:

- Estilos nativos de css,
- Módulos de CSS,
- Styled Components.

Veremos como poner estilos con CSS nativo, crearemos un archivo css por cada componente y/o páginas.

Vamos a crear un archivo App.css para poner estilos al App.jsx. Todo esto lo haremos creando una carpeta llamada css. La idea es colocar el mismos nombre al de jsx y css. Para nuestro caso tenemo el componente App.jsx pues creamos el archivo css App.css, para poder aplicar los estilos lo importamos en el archivo App.jsx.

Gracias a CRA y Vite podemos hacer la importacion del archivo .css.

Estilos generales:

## App.css:

```css
* {
  padding: 0;
  margin: 0;
  box-sizing: border-box;
}

body {
  font-size: 16px;
  font-family: arial, sans-serif;
}

#root {
  display: flex;
  align-items: center;
  justify-content: center;
}

.contenedor {
  width: 90%;
  max-width: 500px;
  padding: 40px;
  margin: 40px;
  border-radius: 2px;
  box-shadow: 0 0 50px rgba(0, 0, 0, 0.1);
}
```

Para usar esa hoja de estilo en el componente debemos usar la instruccioon import y pasarle por comillas las ruta donde esta el archivo css. Reacorar que en React.js no podemos usar la instruccion class para colocar las clases, debemos usar className.

## App.jsx:

```javascript
import { useState } from 'react';
import Usuario from './components/Usuario';
import FormularioSesion from './components/FormularioSesion';
import Counter from './components/Counter';

import './css/App.css';

const App = () => {
  /*
  Creamos el estado inicial de nuestro componente
  y por medio de changeSesion() lo cambiamos:
  */
  const [sesion, changeSesion] = useState(false);
  const cerrarSesion = () => {
    changeSesion(false);
  };
  const iniciarSesion = () => {
    changeSesion(true);
  };

  return (
    <div className="contenedor">
      {sesion ? (
        <div>
          <Usuario />
          <Counter incremento={100} decremento={30} />
          <button onClick={cerrarSesion}>Cerrar Sesión</button>
        </div>
      ) : (
        <div>
          <p>No has iniciado sesion.</p>
          <FormularioSesion iniciarSesion={iniciarSesion} />
        </div>
      )}
    </div>
  );
};

export default App;
```

Ahora vamos a ver como dar estilos al formulario de inicio de sesión. Para eso creamos un nuevo archivo llamado FormularioSesion.css

Creamos los estilo y le colocamos el className a cada elemento.

## FormularioSesion.css:

```css
.formulario {
  display: grid;
  grid-template-rows: 1fr 1fr 1fr;
  gap: 20px;
  margin: 20px 0 0 0;
}

.label {
  font-weight: bold;
  font-size: 14px;
  display: block;
  margin-bottom: 5px;
}

.input {
  width: 100%;
  padding: 10px;
  border: 2px solid #d1d1d1;
  border-radius: 3px;
}

.boton {
  background: #83d394;
  display: inline-block;
  padding: 20px;
  border: none;
  border-radius: 3px;
  font-weight: bold;
  font-family: Arial, sans-serif;
  cursor: pointer;
  transition: 0.3s ease all;
}

.boton:hover {
  background: #44a559;
  color: #fff;
}
```

## FormularioSesion.jsx:

```javascript
import { useState } from 'react';
import '../css/FormularioSesion.css';

// eslint-disable-next-line react/prop-types
const FormularioSesion = ({ iniciarSesion }) => {
  // Definimos los estados inicial de los imput:
  const [user, changeUser] = useState('');
  const [password, changePassword] = useState('');

  // Creamos la funcion global para todos los input:
  // Con e.targe.name accedemos al input:
  const onChange = (e) => {
    if (e.target.name === 'user') {
      changeUser(e.target.value);
    } else if (e.target.name === 'password') {
      changePassword(e.target.value);
    }
  };

  // Funcion que se ejcuta dar click al boton de envia el formulario:
  const onSubmitForm = (e) => {
    // Prevenimos el comportamiento por defecto del formulario:
    e.preventDefault();

    // Hacemos las validaciones:
    if (user === 'leonardo' && password === '123') {
      // Lanzamos la funcion que cambia el estado a true que viene por props.
      iniciarSesion();
    } else {
      alert('Datos incorrectos.');
      // Datos incorrectos limpiamos el formaulario:
      changeUser('');
      changePassword('');
    }
  };

  return (
    <form id="form" onSubmit={onSubmitForm} className="formulario">
      <h1>Inicio de sesión.</h1>
      <div>
        <label htmlFor="user" className="label">
          Usuario
        </label>
        <input
          className="input"
          type="text"
          id="user"
          name="user"
          value={user}
          onChange={onChange}
        />
      </div>
      <div>
        <label htmlFor="password" className="label">
          Contraseña
        </label>
        <input
          className="input"
          type="password"
          id="password"
          name="password"
          value={password}
          onChange={onChange}
        />
      </div>
      <button type="submit" className="boton">
        Iniciar sesión
      </button>
    </form>
  );
};

export default FormularioSesion;
```

Esta forma de trabajar con css es muy facil, pero tiene un problema, tenemos muchos componentes que se repiten, por ejemplo los botones, podemos tener colision de clases y seria tambien poco escalable. Esta bien paa ejemplos pequeños pero para proyectos grandes como que no.

Para evitar colisiones debemos crear una clase para cada componente y eso es poco escalable, la idea es reutilizar codigo, reutilizar componentes.
