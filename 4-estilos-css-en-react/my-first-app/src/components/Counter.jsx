import { useState } from 'react';
// import styles from '../css-modulos/Counter.module.css';
import Boton from '../elements/Boton';

const Counter = (props) => {
  const [counter, changeCounter] = useState(0);

  // eslint-disable-next-line react/prop-types
  const { incremento, decremento } = props;

  const incrementar = (incremento) => {
    changeCounter(counter + incremento);
  };

  const disminuir = (decremento) => {
    changeCounter(counter - decremento);
  };

  return (
    <div>
      <h1>Counter: {counter}</h1>
      {/* <button
        className={styles.boton}
        onClick={() => {
          incrementar(incremento);
        }}
      >
        Incrementar
      </button> */}
      <Boton
        $negro
        $marginTop
        $marginRight
        onClick={() => {
          incrementar(incremento);
        }}
      >
        Incrementar
      </Boton>
      {/* <button
        className={styles.boton}
        onClick={() => {
          disminuir(decremento);
        }}
      >
        Disminuir
      </button> */}
      <Boton
        $negro
        $marginTop
        $marginRight
        onClick={() => {
          disminuir(decremento);
        }}
      >
        Disminuir
      </Boton>
    </div>
  );
};

export default Counter;
