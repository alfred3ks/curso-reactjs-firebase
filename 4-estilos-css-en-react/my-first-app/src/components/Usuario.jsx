import Titulo from './Titulo';
import styled from 'styled-components';

// Componente Usuario:
const Usuario = () => {
  const pais = 'Suiza';
  const amigos = ['Alejandro', 'Manuel', 'Camila'];
  return (
    <div>
      <Titulo usuario="Leonardo" color={'crimson'} edad={34} />
      <Parrafo>Mis amigos son:</Parrafo>
      <ul>
        {amigos.map((amigo, index) => {
          return <li key={index}>{amigo}</li>;
        })}
      </ul>
      {pais && <p>Soy de {pais}.</p>}
      <Parrafo>Que tengas un buen dia</Parrafo>
    </div>
  );
};

export default Usuario;

// Creamos los componentes con styled component:
const Parrafo = styled.p`
  margin: 20px 0;
`;
