import { useState } from 'react';
import Usuario from './components/Usuario';
import FormularioSesion from './components/FormularioSesion';
import Counter from './components/Counter';
import './css/App.css';
import Boton from './elements/Boton';

const App = () => {
  /*
  Creamos el estado inicial de nuestro componente
  y por medio de changeSesion() lo cambiamos:
  */
  const [sesion, changeSesion] = useState(false);
  const cerrarSesion = () => {
    changeSesion(false);
  };
  const iniciarSesion = () => {
    changeSesion(true);
  };

  return (
    <div className="contenedor">
      {sesion ? (
        <div>
          <Usuario />
          <Counter incremento={100} decremento={30} />
          <Boton $largo $marginTop onClick={cerrarSesion}>
            Cerrar Sesión
          </Boton>
        </div>
      ) : (
        <div>
          <FormularioSesion iniciarSesion={iniciarSesion} />
        </div>
      )}
    </div>
  );
};

export default App;
