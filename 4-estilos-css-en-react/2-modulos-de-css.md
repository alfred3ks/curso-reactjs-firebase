# Módulos de CSS:

La segunda forma de poner estilos en React.js es usar los módulos de css.

En React, los módulos de CSS son una forma de modularizar estilos CSS para componentes específicos. Estos módulos permiten el alcance local de estilos, lo que significa que los estilos definidos en un archivo CSS se aplican solo al componente al que pertenecen y no afectan a otros componentes en la aplicación.

Cuando se usa esta técnica, los nombres de clase CSS se generan de manera única para cada componente, lo que evita conflictos con otros estilos en la aplicación. Esto se logra generalmente mediante el uso de convenciones de nomenclatura que son transformadas automáticamente por las herramientas de compilación (como Webpack, Babel o herramientas similares) para garantizar la unicidad de los nombres de clase.

Los archivos de estilos para módulos de CSS suelen tener una convención de nombres específica, por ejemplo, usando la extensión .module.css al final del nombre del archivo para indicar que se trata de un módulo de CSS.

Dentro de estos archivos, los estilos se definen de manera similar a un archivo CSS normal. La diferencia principal radica en que los selectores y nombres de clase se usan como si fueran locales al componente.

Asi podemos poner en cada modulo la misma clase y solo se aplicara a donde lo importamos.

Voy a crear la carpeta css-modulos y creare los archivos ahi, recordar lo normal es crear una carpeta css, esto solo lo hago para poner los ejemplo.

Lo que tenemos que hacer es por ejemplo nombrar asi nuestro archivos:

- FormularioSesion.module.css,
- App.module.css

Ahora para usarlo bastaria con importarlo en nuestro archivo .jsx usando la instrucción import de la siguiente manera:

```javascript
import styles from '../css-modulos/FormularioSesion.module.css';
```

Y para poder aplicar los estilos en el componente:

```javascript
<label htmlFor="password" className={styles.label}>
  Contraseña
</label>
```

Cuando revisamos con el inspector el elemento vemos como coloca las clases:

```html
<div class="_contenedor_1euuj_35">
  <div>
    <form id="form" class="_formulario_1iq47_1">
      <h1>Inicio de sesión.</h1>
      <div>
        <label for="user" class="_label_1iq47_15">Usuario</label
        ><input
          class="_input_1iq47_29"
          type="text"
          id="user"
          name="user"
          value=""
        />
      </div>
      <div>
        <label for="password" class="_label_1iq47_15">Contraseña</label
        ><input
          class="_input_1iq47_29"
          type="password"
          id="password"
          name="password"
          value=""
        />
      </div>
      <button type="submit" class="_boton_1iq47_43">Iniciar sesión</button>
    </form>
  </div>
</div>
```

Asi nunca tendremos colisiones y cada archivo css se aplicara por cada archivo .jsx.

Aqui tambien vemos que podemos hacer una combinación, pero lo ideal es usar una sola opcion, en App.jsx estamos usando css nativo. aunque podriamos usar el archivo App.module.css que tengo que ya he configurado con los estilos.
