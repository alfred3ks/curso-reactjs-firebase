# Styled Component:

Styled Components es una biblioteca de estilizado para React que permite escribir estilos CSS directamente dentro de los componentes de React. En lugar de separar los estilos en archivos CSS independientes, Styled Components utiliza una técnica llamada "CSS-in-JS" para definir estilos en el nivel del componente.

Algunas de las características y ventajas de Styled Components incluyen:

Sintaxis basada en JavaScript: Con Styled Components, los estilos se definen utilizando una sintaxis similar a la de los templates literales de JavaScript, permitiendo escribir CSS directamente en los componentes de React.

Encapsulación de estilos: Los estilos definidos con Styled Components están encapsulados dentro del componente, lo que significa que no afectan a otros elementos en la aplicación. Esto evita colisiones de nombres de clases y hace más sencillo el mantenimiento de estilos.

Dinámica y props-aware: Los estilos pueden ser condicionales o dinámicos basados en props o estados del componente, lo que permite crear componentes más flexibles y adaptables.

Facilidad de mantenimiento: Al definir estilos directamente dentro de los componentes, se facilita la comprensión de la relación entre el código y los estilos asociados. Además, los cambios en los estilos se pueden realizar de manera más localizada y específica.

Para mostrar como usar styled component en la app he borrado todos los estilos para poder usar esta libreria.

Para poder usar styled component lo tenemos que instalar, es una libreia, lo podemos ver en su página web. La instalamos como dependencia del proyecto.

https://styled-components.com/

```bash
npm install styled-components
```

Vamos a ver como usar styled component en el componente Usuario.jsx.

## Usuario.jsx:

```javascript
import Titulo from './Titulo';
import styled from 'styled-components';

// Componente Usuario:
const Usuario = () => {
  const pais = 'Suiza';
  const amigos = ['Alejandro', 'Manuel', 'Camila'];
  return (
    <div>
      <Titulo usuario="Leonardo" color={'crimson'} edad={34} />
      <Parrafo>Mis amigos son:</Parrafo>
      <ul>
        {amigos.map((amigo, index) => {
          return <li key={index}>{amigo}</li>;
        })}
      </ul>
      {pais && <p>Soy de {pais}.</p>}
      <Parrafo>Que tengas un buen dia</Parrafo>
    </div>
  );
};

export default Usuario;

// Creamos los componentes con styled component:
const Parrafo = styled.p`
  margin: 20px 0;
`;
```

Existen varias formas de crear un componente con Styled Component, lo podemos hacer desde el mismo archivo .jsx al final o al inicio, eso da igual o hacerlo en un archivo externo y luego importarlo. Dependera si queremos reusar el componente.

Lo podemos ver en el caso de los botones, creamos una carpeta llamada elements. Vemos como creamos un componente Boton.jsx con styled component, y lo usamos en el App.jsx en el boton de cerrar sesion.

## Boton.jsx:

```css
import styled from 'styled-components';

const Boton = styled.button`
  background: #83d394;
  display: inline-block;
  padding: 20px;
  border: none;
  border-radius: 3px;
  font-weight: bold;
  font-family: Arial, sans-serif;
  cursor: pointer;
  transition: 0.3s ease all;

  // Hover:
  &:hover {
    background: #44a559;
    color: #fff;
  }
`;

export default Boton;
```

## App.jsx:

```javascript
import Boton from './elements/Boton';

<Boton onClick={cerrarSesion}>Cerrar Sesión</Boton>;
```

Ahora ya tenemos un boton que lo podemos usar donde queramos, reutilizando el código.

Vamos a colocarlos en en el FormularioSesion.jsx:

## FormularioSesion.jsx:

```javascript
import Boton from '../elements/Boton';

<Boton type="submit">Iniciar sesión</Boton>;
```

Vamos a cambiar tambien los botones del contador.

## Counter.jsx:

```javascript
import Boton from '../elements/Boton';

<Boton
  onClick={() => {
    incrementar(incremento);
  }}
>
  Incrementar
</Boton>
<Boton
  onClick={() => {
    disminuir(decremento);
  }}
>
  Disminuir
</Boton>
```

Ahora veremos un tema interesante, ahora todos los botones son iguales, pero supongamos que queremos hacer variaciones de nuestros botones, los dos botones del contador vamos a crear una variente, pues nuestro componente boton podemos pasar propiedades. props.

Vemos el archivo Counter.jsx:

## Counter.jsx:

```javascript
<Boton
  $negro
  onClick={() => {
    incrementar(incremento);
  }}
>
  Incrementar
</Boton>
```

Pasamos una propiedad, es un nombre arbitrario, luego en el componente Boton debemos en el import poner {css} junto con styled.

Y ya dentro del boton definimos las props:

## Boton.jsx:

```javascript
import styled, { css } from 'styled-components';

// Accedemos a las props:
${(props) =>
  props.$negro && css`
    background: #000;
    color: #fff;
`}
```

Esto es una pasada...
Tambien podemos trabajar con condicionales. En el boton de cerrar sesion vamos a ponerle que se vaya a todo lo ancho poniendo la consicion que si tiene la propiedad largo.

## Boton.jsx:

```javascript
// Condicional: Ancho dinamico y depende de las props:
  width: ${(props) => (props.$largo ? '100%' : 'auto')};
```

## App.jsx:

```javascript
<Boton $largo onClick={cerrarSesion}>
  Cerrar Sesión
</Boton>
```

Vemos como se va a todo lo largo de su contenedor. Una pasada!!!

NOTA: Aqui vemos que las props se la tenemos que pasar con el simbolo de dolar. Ver $largo, $negro.

Ahora tambien vemos como podemos poner propiedades dinamicas que solo usaremos si las pasamos por props, hemos puesto un marginTop y marginRight.

## Boton.jsx:

```javascript
  // Ponemos margin a los botones de manera dinamica:
  ${(props) =>
    props.$marginTop &&
    css`
      margin-top: 10px;
    `}
  ${(props) =>
    props.$marginRight &&
    css`
      margin-right: 10px;
    `}
```

## App.jsx:

```javascript
<Boton $largo $marginTop onClick={cerrarSesion}>
```

## Counter.jsx:

```javascript
<Boton
  $negro
  $marginTop
  $marginRight
  onClick={() => {
    incrementar(incremento);
  }}
>
  Incrementar
</Boton>
```

Muy muy interesante...
