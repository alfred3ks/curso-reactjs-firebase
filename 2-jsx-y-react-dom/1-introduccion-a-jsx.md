# Introducción a JSX:

Empezaremos a mostrar los ejemplos en el proyecto creado my-first-app.

El código JSX es código que parece html dentro de código JavaScript.
JSX es una extensión de sintaxis de JavaScript que nos permite mezclar JS y HTML (XML), de ahí su nombre JavaScript XML.

Esta extensión nos facilita la vida pues nos permite escribir un código más limpio, sin tantas repeticiones (DRY), y con muy pocos factores o condiciones a tener en cuenta.

Lo podemos ver en el componente Introduccion.jsx donde vemos que el componente retorn un HTML, esto no es HTML es código JSX.

```jsx
const App = () => {
  return <h1>Hola React.js</h1>;
};

export default App;
```

Lo bueno de usar jsx es que dentro de ese código que parece HTML podemos ejecutar código JavaScript. Vemos como pasar una variable e inyectarla usando las llaves {}.

Si necesitamos agregar más de un elemento, no lo podemos hacer, ya que un componente debe retornar un solo elemento, para eso debemos envolver o un div o en lo que se llama un fragmento.

Si queremos poner clases debemos usar las instruccion className='title', no podemos usar la palabra class ya que esta es una palabra reservada de JavaScript.

Tambien podemos poner estilos en linea, para eso usamos la instrucción style={{ color: 'red' }}. Pasamos un objeto con todas las propiedades.

```jsx
const App = () => {
  const nombre = 'Alfred0';
  return (
    <>
      <h1 className="title">Hola {nombre}</h1>
      <p style={{ color: 'red' }}>Que tengas un buen dia.</p>
    </>
  );
};

export default App;
```
