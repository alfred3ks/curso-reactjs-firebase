# Condicionales en JSX:

Para ver los condicionales he creado un componente llamado Condicionales.jsx. Aqui podemos ver una simulación de inicio de sesion.

En este ejemplo podemos ver que podemos usar un condicional muy usado llamado el ternario, en funcion de si un valor es true mostrara algo y false mostrara otra cosa.

Tambien podemos ver que podemos mostrar o no un elemento usando el operador and (&&), si el primer valor es false toda dara false y no se mistrara nada, lo podemos ver en el elemento donde mostraremos el pais, eso es muy usado por si el usuario nos debe pasar algo y no lo hace.

## Condicionales:

```jsx
const Condicionales = () => {
  const nombre = 'Carlos';
  const color = 'blue';
  const sesion = true;
  const pais = '';

  return (
    <>
      {sesion ? (
        <>
          <p>Has Iniciado Sesion</p>
          <h1 className="title">Hola {nombre}</h1>
          <p style={{ color: color }}>Que tengas un buen dia.</p>
          {pais && <p>Tu eres de {pais}</p>}
        </>
      ) : (
        <p>No has iniciado sesion</p>
      )}
    </>
  );
};

export default Condicionales;
```

```jsx
{
  pais && <p>Tu eres de {pais}</p>;
}
```

Para el caso de pais, tanto como si es null, undefined, o una cadena vacia, no se mostrara nada al evaluarse la expresion {}. Esto tiene que ver con los valores en javascript que evaluan a false.
