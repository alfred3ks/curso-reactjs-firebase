const Arreglos = () => {
  const nombre = 'Leonardo';
  const color = 'blue';
  const sesion = true;
  const pais = 'Polonia';
  const amigos = ['Alejandro', 'Manuel', 'Camila'];

  return (
    <>
      {sesion ? (
        <>
          <p>Has Iniciado Sesion</p>
          <h1 className="title">Hola {nombre}</h1>
          <p style={{ color: color }}>Que tengas un buen dia.</p>
          {pais && <p>Tu eres de {pais}.</p>}
          <p>Mis amigos: {amigos}</p>
          <p>Mis amigos: {amigos.join(', ')}</p>
          <p>Mis amigos:</p>
          <ul>
            {amigos.map((amigo, index) => {
              return <li key={index}>{amigo}</li>;
            })}
          </ul>
        </>
      ) : (
        <p>No has iniciado sesion</p>
      )}
    </>
  );
};

export default Arreglos;
