const Condicionales = () => {
  const nombre = 'Carlos';
  const color = 'blue';
  const sesion = true;
  const pais = '';

  return (
    <>
      {sesion ? (
        <>
          <p>Has Iniciado Sesion</p>
          <h1 className="title">Hola {nombre}</h1>
          <p style={{ color: color }}>Que tengas un buen dia.</p>
          {pais && <p>Tu eres de {pais}</p>}
        </>
      ) : (
        <p>No has iniciado sesion</p>
      )}
    </>
  );
};

export default Condicionales;
