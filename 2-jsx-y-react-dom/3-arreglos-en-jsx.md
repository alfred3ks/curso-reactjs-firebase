# Arreglos en JSX:

Lo que veremos como podemos mostrar arreglos en JSX. Si tenemos un arreglo y se lo pasamos a JSX dentro de las llaves {amigos} vemos como nos muestra el arreglo en el dom, AlejandroManuelCamila:

```jsx
const amigos = ['Alejandro', 'Manuel', 'Camila']

<p>Mis amigos: {amigos}</p>
```

Podriamos usar algun método de array para mostrarlo mejor, por ejemplo separados de comas:

```jsx
const amigos = ['Alejandro', 'Manuel', 'Camila']

<p>Mis amigos: {amigos.join(', ')}</p>
```

Tambien podemos recorre el arreglo y mostrarlo como una lista, para eso debemos usar un método para reocrrer el arreglo y mostrarlo:

```jsx
const amigos = ['Alejandro', 'Manuel', 'Camila']

<p>Mis amigos:</p>
  <ul>
    {amigos.map((amigo, index) => {
      return <li key={index}>{amigo}</li>;
    })}
  </ul>
```

Cuando recorremos arreglos en JSX siempre debemos pasarle un atributo key, es un identificador unico que usa react para identificar cada elemento del arreglo. Si no lo hacemos vemos que por consola nos da ese warning.
