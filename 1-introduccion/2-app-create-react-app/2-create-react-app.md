# Creación de una app usando create-react-app o vite:

Create React App es una herramienta oficial creado por la gente de Facebook o Meta para poder iniciar proyectos de React.js, nos crea un esqueleto de app para que podamos iniciar nuestro proyecto.

Lo podemos ver en la documentación de React.js:

https://create-react-app.dev/docs/getting-started

Vamos a crear nuestro primer proyecto por consola:

```bash
npx create-react-app my-first-app
cd my-first-app
npm start
```

A ver esto es algo lento, para proyectos a dia de hoy se usa vite, lo podemos ver en su documentacion, yo voy a crear los proyectos con vite que es mas rapido.

https://vitejs.dev/

npm create vite@latest my-first-app

Solo debemos seguir los pasos e ir respondiendo las preguntas que nos dice el instalador.

Lo que debemos hacer al iniciar un proyecto es hacer limpia de archivos para comenzar con el proyecto desde cero.

Vemos en el package.json el script de arranque de la app:

```bash
npm run dev
```

Con esto ya vemos como tener un esqueleto limpio para comenzar el desarrollo de una app.
