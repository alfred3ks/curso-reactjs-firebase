import { useState } from 'react';
import Usuario from './components/Usuario';
import FormularioSesion from './components/FormularioSesion';
import './css/App.css';
import Boton from './elements/Boton';
import CounterUseReducer from './components/CounterUseReducer';
import Blog from './components/Blog';

const App = () => {
  /*
  Creamos el estado inicial de nuestro componente
  y por medio de changeSesion() lo cambiamos:
  */
  const [sesion, changeSesion] = useState(true);
  const cerrarSesion = () => {
    changeSesion(false);
  };
  const iniciarSesion = () => {
    changeSesion(true);
  };

  return (
    <div className="contenedor">
      {sesion ? (
        <div>
          <Usuario />
          <Blog />
          <CounterUseReducer />
          <Boton $largo $marginTop onClick={cerrarSesion}>
            Cerrar Sesión
          </Boton>
        </div>
      ) : (
        <div>
          <FormularioSesion iniciarSesion={iniciarSesion} />
        </div>
      )}
    </div>
  );
};

export default App;
