import { useEffect, useState } from 'react';
// import styles from '../css-modulos/Counter.module.css';
import Boton from '../elements/Boton';

const Counter = (props) => {
  const [counter, changeCounter] = useState(0);

  // Implementamos useEffect():
  // Este hooks se ejecuta en cada ciclo de renderizado
  // componentDidMount y componentDidUpdate()
  useEffect(() => {
    console.log('El componente se renderizo.');
  });

  /*
  Si queremos que se ejecute solo cuando carga por primera vez,
  necesitamos pasarle un segundo parametro. Es un arreglo de dependencias [] vacio,
  por ejemplo conectarnos a una API.
  Este es el equivalente al componentDidMount().
  */
  useEffect(() => {
    console.log('Se ejecuta solo en el primer render.');
    // Conectamos a una API.
  }, []);

  /*
  Ejecutaremos este efecto solo cuando el contador cambie,
  para eso usamos el arreglo de dependencias,
  Solo cuando cambia el estado de la dependencia que pasamos en
  el arreglo.
  Podemos pasar mas dependencias separadas por comas.
  */
  useEffect(() => {
    console.log('Se ejecuta solo cuando cambia el estado del contador.');
  }, [counter]);

  /*
  Asi se ejecuta cuando se desmonta el componente.
  Para eso tenemos que retornar un funcion.
  Tambien se pone el arreglo vacio para que se ejecute una vez.
  */
  useEffect(() => {
    return () => {
      console.log('Adios componente!!!');
      // Desconectamos de la API.
    };
  }, []);

  // eslint-disable-next-line react/prop-types
  const { incremento, decremento } = props;

  const incrementar = (incremento) => {
    changeCounter(counter + incremento);
  };

  const disminuir = (decremento) => {
    changeCounter(counter - decremento);
  };

  return (
    <div>
      <h1>Counter: {counter}</h1>
      <Boton
        $negro
        $marginTop
        $marginRight
        onClick={() => {
          incrementar(incremento);
        }}
      >
        Incrementar
      </Boton>
      <Boton
        $negro
        $marginTop
        $marginRight
        onClick={() => {
          disminuir(decremento);
        }}
      >
        Disminuir
      </Boton>
    </div>
  );
};

export default Counter;
