// eslint-disable-next-line react/prop-types
const GetUserObj = ({ usuario }) => {
  // eslint-disable-next-line react/prop-types
  const { name, color, age } = usuario;
  return (
    <h2 style={{ color: color }} className="title">
      Hello soy {name} y tengo {age} años.
    </h2>
  );
};

export default GetUserObj;
