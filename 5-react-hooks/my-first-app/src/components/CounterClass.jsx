import { Component } from 'react';

// Creamos una clase:
class CounterClass extends Component {
  // metodo para ver las props/estado:
  constructor(props) {
    super(props);

    // Asi establecemos el estado:
    this.state = { counter: 0 };
  }

  // 📌 Metodos de ciclo de vida:
  componentDidMount() {
    /*
    Para cuando se monta el contador ejecutamos este método.
    Este método es muy cuando cuando por ejemplo queremos hacer una llamada a un servicio, a una API.
    */
    console.log('El contador se monto en el DOM.');
    // Llamamos a un API...
  }

  componentDidUpdate(propsAnterior, stateAnterior) {
    /*
    Muy usado cuando el componente se vuelve a renderizar porque su estado cambio. Por ejemplo cuando pulsamos los botones de incremento y decremento.
    Podriamos saber las propiedades anteriores del componente antes de actualizarlo y su estado, pasamos por parametros esos valores.
    */
    console.log('El contador se actualizo.');
    console.log(propsAnterior);
    console.log(stateAnterior);
  }

  componentWillUnmount() {
    /*
    Este método nos permite ejecutar código antes de quitar el componente del DOM.
    Lo probamos por ejemplo cerrando la sesion.
    Tambien muy usado para cerrar conexion a la API.
    */
    console.log('Adios componente!!!');
  }

  // Métodos del contador
  incrementar(incremento) {
    // Asi cambiamos el estado:
    this.setState((estadoAnterior) => {
      return {
        counter: estadoAnterior.counter + incremento,
      };
    });
  }

  disminuir(decremento) {
    this.setState((estadoAnterior) => {
      return {
        counter: estadoAnterior.counter - decremento,
      };
    });
  }

  // Usamos el metodo render() para retornar jsx
  render() {
    return (
      <div>
        <h1>Counter: {this.state.counter}</h1>
        <button
          onClick={() => {
            // eslint-disable-next-line react/prop-types
            this.incrementar(this.props.incremento);
          }}
        >
          Incrementar
        </button>
        <button
          onClick={() => {
            // eslint-disable-next-line react/prop-types
            this.disminuir(this.props.decremento);
          }}
        >
          Disminuir
        </button>
      </div>
    );
  }
}

export default CounterClass;
