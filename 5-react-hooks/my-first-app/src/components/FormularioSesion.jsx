import { useState } from 'react';
import styles from '../css-modulos/FormularioSesion.module.css';
import Boton from '../elements/Boton';

// eslint-disable-next-line react/prop-types
const FormularioSesion = ({ iniciarSesion }) => {
  // Definimos los estados inicial de los imput:
  const [user, changeUser] = useState('');
  const [password, changePassword] = useState('');

  // Creamos la funcion global para todos los input:
  // Con e.targe.name accedemos al input:
  const onChange = (e) => {
    if (e.target.name === 'user') {
      changeUser(e.target.value);
    } else if (e.target.name === 'password') {
      changePassword(e.target.value);
    }
  };

  // Funcion que se ejcuta dar click al boton de envia el formulario:
  const onSubmitForm = (e) => {
    // Prevenimos el comportamiento por defecto del formulario:
    e.preventDefault();

    // Hacemos las validaciones:
    if (user === 'leonardo' && password === '123') {
      // Lanzamos la funcion que cambia el estado a true que viene por props.
      iniciarSesion();
    } else {
      alert('Datos incorrectos.');
      // Datos incorrectos limpiamos el formaulario:
      changeUser('');
      changePassword('');
    }
  };

  return (
    <form id="form" onSubmit={onSubmitForm} className={styles.formulario}>
      <h1>Inicio de sesión.</h1>
      <div>
        <label htmlFor="user" className={styles.label}>
          Usuario
        </label>
        <input
          className={styles.input}
          type="text"
          id="user"
          name="user"
          value={user}
          onChange={onChange}
        />
      </div>
      <div>
        <label htmlFor="password" className={styles.label}>
          Contraseña
        </label>
        <input
          className={styles.input}
          type="password"
          id="password"
          name="password"
          value={password}
          onChange={onChange}
        />
      </div>
      {/* <button type="submit" className={styles.boton}>
        Iniciar sesión
      </button> */}
      <Boton type="submit">Iniciar sesión</Boton>
    </form>
  );
};

export default FormularioSesion;
