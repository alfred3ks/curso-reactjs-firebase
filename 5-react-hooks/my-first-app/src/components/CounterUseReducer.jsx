import { useReducer } from 'react';
import Boton from '../elements/Boton';

const CounterUseReducer = () => {
  /*
  Asi creamos un reducer, recibe por parametro un reducer, un valor inicial del estado,
  y extraemos del useReducer el estado y una funcion dispatch().
  La funcion dispatch() recibe por parametro una accion, la cual es un objeto {},
  esta funcion la ejecutamos dentro del evento onClick={} para nuestro caso del boton.
  {
    tipo: 'INCREMENTAR'
  }
  {
    tipo: 'DISMINUIR'
  }
  El counterInit es otro objeto con el valor inicial de la variable del estado.
  El reducer es una función, recibe el estado y el action, esta funcion reducer estara escuchando el tipo de accion que le estamos ejecutando
  con el dispatch().
  Internamente tenemos un switch() que recibe el tipo de la acción y por cada accion retornaremos otro objeto con lo que queremos hacer.
  Esto es muy potente cuando tenemos muchas acciones a ejecutar en un componente.
  Vemos que agregamos un reset y es muy facil.
  */

  // Estado inicial:
  const counterInit = { counter: 0 };

  // Reducer: Funcion que recibe el tipo y la accion:
  const reducer = (state, action) => {
    switch (action.type) {
      case 'INCREMENTAR':
        return {
          counter: state.counter + 1,
        };
      case 'DISMINUIR':
        return {
          counter: state.counter - 1,
        };
      case 'REINICIAR':
        return {
          counter: 0,
        };
      default:
        return state;
    }
  };

  const [state, dispatch] = useReducer(reducer, counterInit);

  return (
    <div>
      <h1>Counter:{state.counter}</h1>
      <Boton
        $negro
        $marginTop
        $marginRight
        onClick={() => {
          dispatch({
            type: 'INCREMENTAR',
          });
        }}
      >
        Incrementar
      </Boton>
      <Boton
        $negro
        $marginTop
        $marginRight
        onClick={() => {
          dispatch({
            type: 'DISMINUIR',
          });
        }}
      >
        Disminuir
      </Boton>
      <Boton
        $negro
        $marginTop
        $marginRight
        onClick={() => {
          dispatch({
            type: 'REINICIAR',
          });
        }}
      >
        Resetear
      </Boton>
    </div>
  );
};

export default CounterUseReducer;
