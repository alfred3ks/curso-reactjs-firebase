import { useState, useEffect } from 'react';

const useObtenerArticulos = () => {
  const [articulos, setArticulos] = useState([]);
  const [cargando, setCargando] = useState(true);
  // Ejecutamos solo la primera vez:
  useEffect(() => {
    // Nos conectamos a una API: Simulación:
    setTimeout(() => {
      // Establecemos los articulos
      setArticulos([
        { id: 1, titulo: 'Título del primer artículo' },
        { id: 2, titulo: 'Título del segundo artículo' },
        { id: 3, titulo: 'Título del tercer artículo' },
      ]);
      setCargando(false);
    }, 3000);
  }, []);

  return [articulos, cargando];
};

export default useObtenerArticulos;
