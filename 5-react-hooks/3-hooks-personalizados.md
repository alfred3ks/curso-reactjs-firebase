# Hooks personalizados:

Vamos a ver como crear hooks personalizados, osea crear nuestros propios hooks.

Para ver como haremos esto en la aplicacion que llevamos vamos a implementar un pequeño blog.
Vamos a crear el componente del blog, Blog.jsx

Los hooks personalizados nos permiten poder usar ese lógica en otras partes de nuestra app.

Para eso debemos crear una carpeta llamada hooks.

Un hooks personalizado es una funcion que retorna un arreglo o objetos, por convencion usamos la palabra use, ademas podemos usar en su interior otros hooks. Para nuestro caso devolvemos los articulos y el estado de cargando. Luego para usarlo lo hacemos como cualquier otro hooks. Un hooks personalizado no retorna codigo jsx. Esto seria el equivalente a usar funciones solo que con funciones no podemos usar en su interior hooks.

## useObtenerArticulos.jsx:

```javascript
import { useState, useEffect } from 'react';

const useObtenerArticulos = () => {
  const [articulos, setArticulos] = useState([]);
  const [cargando, setCargando] = useState(true);
  // Ejecutamos solo la primera vez:
  useEffect(() => {
    // Nos conectamos a una API: Simulación:
    setTimeout(() => {
      // Establecemos los articulos
      setArticulos([
        { id: 1, titulo: 'Título del primer artículo' },
        { id: 2, titulo: 'Título del segundo artículo' },
        { id: 3, titulo: 'Título del tercer artículo' },
      ]);
      setCargando(false);
    }, 3000);
  }, []);

  return [articulos, cargando];
};

export default useObtenerArticulos;
```

## Blog.jsx:

```javascript
import styled from 'styled-components';
import useObtenerArticulos from '../hooks/useObtenerArticulos';

const Blog = () => {
  const [articulos, cargando] = useObtenerArticulos();
  return (
    <ContenedorBlog>
      <Titulo>Blog</Titulo>
      {cargando ? (
        <p>Cargando...</p>
      ) : (
        <div>
          {articulos.map((articulo) => {
            return <Articulo key={articulo.id}>{articulo.titulo}</Articulo>;
          })}
        </div>
      )}
    </ContenedorBlog>
  );
};

export default Blog;

// Damos estilos con styled component:
const ContenedorBlog = styled.div`
  margin: 40px 0 20px 0;
`;

const Titulo = styled.h2`
  margin-bottom: 10px;
`;

const Articulo = styled.p`
  padding: 10px 0;
  margin-bottom: 10px;
  border-bottom: 1px solid #ccc;
`;
```

Ahora este hook personalizado lo podemos usar donde queramos, esto es mas funcional.
