# Hook useEFfect:

Vamos a ver un hook muy usado. Este es el hook de useEffect.

Este hook nos permite es el de ciclo de vida del componente. Los Life Cycle Methods. Métodos del ciclo del vida del componente.

Vamos a ver en el componente Counter.jsx como implementar useEffect(). Este hooks es el equivalente a los métodos de ciclo de vida que vimos en el componente de clases.

Ahora vamos a ver una reglas para implementar un hook:

- Los tenemos que usar siempre dentro del componente funcional, nunca fuer.
- Los hooks no se pueden usar dentro de ciclos o condicionales.

useEffect es un hook en React que permite realizar efectos secundarios en componentes funcionales. Estos efectos secundarios pueden ser, por ejemplo, peticiones a servidores, manipulación del DOM, suscripciones a eventos, actualización de componentes, entre otros.

La función useEffect se ejecuta después de que se renderiza el componente y después de que cualquier actualización de este ha tenido lugar. Toma dos argumentos: una función y, opcionalmente, un array de dependencias.

La función que se pasa como primer argumento a useEffect será ejecutada después de que el componente se renderice por primera vez y luego, cada vez que ocurra una actualización, a menos que se especifiquen dependencias para controlar cuándo debe ejecutarse esa función.

Si se proporciona un array de dependencias como segundo argumento, useEffect se ejecutará solamente si alguna de las dependencias cambia entre renderizaciones. Si no se pasa un array de dependencias, la función se ejecutará en cada renderizado.

useEffect() es una función, pero recibe siempre un parametro, y este parametro es otra funcion. Vamos implementarlo en nuestro componente Counter.jsx y vemos que cuando se renderiza se dispara este hook y tambien cuando pulsamos los botones del contador porque cada vez se renderiza denuevo el componente.

Aqui lo que tenemos que ver que cuando cambia el estado se renderiza el todo el componente. Por eso podemos usar useEffect() cada vez que hagamos un render en pantalla.

Vamos a ver todos los casos comunes de implementación.

## Counter.jsx:

```javascript
// Implementamos useEffect():
// Este hooks se ejecuta en cada ciclo de renderizado
// componentDidMount y componentDidUpdate()
useEffect(() => {
  console.log('El componente se renderizo.');
});
/*
  Si queremos que se ejecute solo cuando carga por primera vez,
  necesitamos pasarle un segundo parametro. Es un arreglo de dependencias [] vacio,
  por ejemplo conectarnos a una API.
  Este es el equivalente al componentDidMount().
  */
useEffect(() => {
  console.log('Se ejecuta solo en el primer render.');
  // Conectamos a una API.
}, []);
/*
  Ejecutaremos este efecto solo cuando el contador cambie,
  para eso usamos el arreglo de dependencias,
  Solo cuando cambia el estado de la dependencia que pasamos en
  el arreglo.
  Podemos pasar mas dependencias separadas por comas.
  */
useEffect(() => {
  console.log('Se ejecuta solo cuando cambia el estado del contador.');
}, [counter]);
/*
  Asi se ejecuta cuando se desmonta el componente.
  Para eso tenemos que retornar un funcion.
  Tambien se pone el arreglo vacio para que se ejecute una vez.
  */
useEffect(() => {
  return () => {
    console.log('Adios componente!!!');
    // Desconectamos de la API.
  };
}, []);
```
